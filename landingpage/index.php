<?php
  require_once __DIR__ . '/src/config/router/template.php';
  require_once __DIR__ . '/src/config/router/config.php';
  require_once __DIR__ . '/src/config/router/router.php';

  $router = new Router();

  echo $router->run();
?>