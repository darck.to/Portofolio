const host = window.location.protocol + '//' + window.location.host;

// Crea un modal
export function modal(incoming) {
  //DESTRUYE VIEJOS MODALES
  if ($('.modal').length) {
    $('.modal').fadeOut('fast', function() {
      $(this).remove()
    })
  }
  //CREA UN MODAL NUEVO
  const modal = `
      <div class="modal">
          <div class="modal-background"></div>
          <div class="modal-content is-relative">
            <ion-icon class="modal-exit has-color-red is-clickable is-size-2" name="close-circle"></ion-icon>
            <div class="card has-background-white" id='modalContent'>${incoming}</div></div>
          <div class="modal-absolute"></div></div>
  `;
  $('body').prepend(modal);
  $('.modal').css('z-index','9000');
  $('.modal').toggleClass("is-active");
  $('.modal-exit').on('click', function(){
      $('.modal').fadeOut('fast', function() {
          $(this).remove()
      })
  });
  $('.modal-background').on('click', function(){
      $('.modal').fadeOut('fast', function() {
          $(this).remove()
      })
  })
}

//CIERRAMODAL
export function modax(e) {
  //DESTRUYE VIEJOS MODALES
  if ($('.modal').length) {
    $('.modal').fadeOut('fast', function() {
      $(this).remove()
    })
  }
}

// LOADER
export function loader(flag = true,time = 200) {
  setTimeout(() => {
    if (!flag) {
      // Remove loading-background and loading from DOM
      $('.loading-background').remove();
      $('.loading').remove();
      console.log('remove')
    } else {
      const loader = `
        <div class="loading-background"></div>
        <div class="loading"></div>
      `;
      $('body').prepend(loader);
      console.log('add')
    }
  }, time);
}