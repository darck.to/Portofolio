import * as plug from '../vendor/plugins.js';
import getHost from '../lib/const.js';

$(function() {
    'use strict';
    const host = getHost();
    plug.loader();

    // mandamos llamar home/main
    $.ajax({
        url: host + '/home/main/',
        contentType: 'application/json',
        type: 'POST',
        dataType: 'html',
        success: function(response, textStatus, xhr) {
            plug.loader(false);
            $('#mainContainer').html(response);
            // Auto escribir
            autoEscribre();

            // Atrapamos los a de /blog endpoint
            $('.blog-link, .proy-link').on('click', function(e) {
                // Carga el loader
                plug.loader();
                e.preventDefault();
                const id = $(this).data('id');
                const ruta = $(this).data('ruta');
                // carga /home/source con $.post()
                $.ajax({
                    url: host + '/home/source/',
                    contentType: 'application/json',
                    type: 'POST',
                    dataType: 'html',
                    success: function(data, textStatus, xhr) {
                        if (data) {
                            // Carga un modal con el contenido de /blog/id
                            $.ajax({
                                url: host + ruta + id,
                                contentType: 'application/json',
                                type: 'POST',
                                dataType: 'html',
                                success: function(response, textStatus, xhr) {
                                    plug.modal(response);
                                    (ruta === '/proyectos/item/') ? $('.modal-content').addClass('is-90') : '';
                                    plug.loader(false,1000);
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    console.log(xhr.responseText);
                                    plug.modal(xhr.responseText);
                                    plug.loader(false,1000);
                                }
                            });
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        plug.loader(false,2500);
                        console.log(xhr.responseText);
                        plug.modal(xhr.responseText);
                    }
                });
            });

            // Listenner que de acuerdo al punto de scroll activa el link del menu
            $(window).scroll(function() {
                const scrollDistance = $(window).scrollTop();
                $('.hero-scroll').each(function(i) {
                    if ($(this).position().top <= scrollDistance) {
                        $('.menu-list a.active').removeClass('active');
                        $('.menu-list a').eq(i).addClass('active');
                    }
                });
            }).scroll();

        },
        error: function(xhr, textStatus, errorThrown) {
            plug.loader(false,1000);
            console.log(xhr.responseText, textStatus, errorThrown);
        }
    });

    function autoEscribre() {
        const text = ['Hola, soy un desarrollador web.', 'Diseño software.', 'Me gusta aprender.', 'Entusiasta de la tecnología.', 'Me gusta el café!', 'Me gusta el café!', 'Me gusta el café!'];
        let i = 0;
        let j = 0;
        let currentText = [];
        let isDeleting = false;
        let speed = 100;
        let textContainer = document.querySelector('.auto-script');
        textContainer.innerHTML = '';
        type();
        function type() {
            if (i < text.length) {
                if (!isDeleting && j <= text[i].length) {
                    currentText.push(text[i][j]);
                    textContainer.innerHTML = currentText.join('');
                    j++;
                    if (j === text[i].length) {
                        isDeleting = true;
                        speed = 100;
                    }
                } else if (isDeleting && j <= text[i].length) {
                    currentText.pop();
                    textContainer.innerHTML = currentText.join('');
                    j--;
                    if (j === 0) {
                        isDeleting = false;
                        i++;
                        speed = 100;
                        if (i === text.length) {
                            i = 0;
                        }
                    }
                }
            }
            setTimeout(type, speed);
        }
    }
        

}());