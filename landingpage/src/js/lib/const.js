// Obtenemos host
function getHost() {
    return window.location.protocol + '//' + window.location.host;
}

export default getHost;