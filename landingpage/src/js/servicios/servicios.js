import getHost from '../lib/const.js';
import * as plug from '/src/js/vendor/plugins.js';

$(function() {
    'use strict';
    const host = getHost();
    plug.loader();

    // mandamos llamar servicios/main
    $.ajax({
        url: host + '/servicios/main/',
        contentType: 'application/json',
        type: 'POST',
        dataType: 'html',
        success: function(response, textStatus, xhr) {
            $('#mainContainer').html(response);
            plug.loader(false,1000);
            $('.serv-link').on('click', function(e) {
                plug.loader();
                e.preventDefault();
                const url = $(this).attr('href');
                // Reload page to window.location.url
                window.location.href = url;
            });
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(xhr.responseText);
            plug.loader(false,2500);
        }
    });
        

}());