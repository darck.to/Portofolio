import getHost from '../lib/const.js';

$(function() {
    'use strict';
    const host = getHost();
    // Listener para imagenes inside content class
    $('.content img').on('click', function() {
        const src = $(this).attr('src');
        const alt = $(this).attr('alt');
        const modal = `
            <div class="modal is-active">
                <div class="modal-background"></div>
                <div class="modal-content is-relative">
                    <ion-icon class="modal-close size="large" has-color-red is-clickable is-size-2" name="close-circle"></ion-icon>
                    <p class="image">
                        <img src="${src}" alt="${alt}">
                    </p>
                </div>
            </div>
        `;
        $('body').append(modal);
        // Click outside image to close modal
        $('.modal-background').on('click', function() {
            $('.modal').fadeOut('fast', function() {
                $(this).remove()
            })
        });
        $('.modal-close').on('click', function() {
            $('.modal').fadeOut('fast', function() {
                $(this).remove()
            })
        });
    });

}());