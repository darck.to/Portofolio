// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.
import * as plug from '/src/js/vendor/plugins.js';
import getHost from '/src/js/lib/const.js';

$(function() {
  'use strict';
  const host = getHost();
  /* particlesJS.load(@dom-id, @path-json, @callback (optional)); */
  particlesJS.load('particles-js', host + '/src/assets/particles/particles.json');

  // Listener para navegar a home reloading page
  $('.nav-home').on('click', function(e) {
      e.preventDefault();
      window.location.href = host;
  });
  // Listener para mostrar el menu
  $('.nav-burguer').on('click', function() {
      $('.nav-box').toggleClass('is-active');
  });
  // Listener para ocultar el menu cuando presionamos fuera de el o en .nav-burguer
  $(document).on('click', function(e) {
      if (!$(e.target).closest('.nav-box').length && !$(e.target).closest('.nav-burguer').length) {
          $('.nav-box').removeClass('is-active');
      }
  });

  // Implementar smooth scroll para los links
  $('.menu-list .nav-item').on('click', function(e) {
    const path = window.location.pathname;
    // Cambiamos el href de los links de menu-list de acuerdo a la url actual
    switch (path) {
    case '/':
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 1200, 'linear');
        break;
    case '/proyectos/':
        $('.menu-list a[href="#blog-nav"]').attr('href', '\/blog\/');
        $('.menu-list a[href="#acerca-nav"]').attr('href', host);
        $('.menu-list a[href="#proyectos-nav"]').addClass('active');
        break;
    case '/blog/':
        $('.menu-list a[href="#proyectos-nav"]').attr('href', '\/proyectos\/');
        $('.menu-list a[href="#acerca-nav"]').attr('href', host);
        $('.menu-list a[href="#blog-nav"]').addClass('active');
    case '/servicios/':
        $('.menu-list a[href="#proyectos-nav"]').attr('href', '\/proyectos\/');
        $('.menu-list a[href="#blog-nav"]').attr('href', '\/blog\/');
        $('.menu-list a[href="#acerca-nav"]').attr('href', host);
    default:
        $('.menu-list a[href="#acerca-nav"]').attr('href', host);
        $('.menu-list a[href="#blog-nav"]').attr('href', '\/blog\/');
        $('.menu-list a[href="#proyectos-nav"]').attr('href', '\/proyectos\/');
        break;
    }
    // Background para menu-list al hacer clicj en un link
    $('.menu-list a').removeClass('active');
    $(this).addClass('active');
  });
  
});
