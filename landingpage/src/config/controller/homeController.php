<?php
    class homeController {
        public function home() {
            // Carga template main
            return Template::home(
                'Portfolio mrvg.DEV',
                'main/main'
            );
        }
        // Carga template de la página principal
        public function main() {
            // Carga el template de la página main
            $template = require __DIR__ . '/./home/home-default.controller.php';
            $template = json_decode($template);
            $saludo = $template->greeting;
            $message1 = $template->message1;
            $message2 = $template->message2;
            $proyectos = $template->proyectos;
            $entradas = $template->blog;
            $contacto1 = $template->contact1;
            $contacto2 = $template->contact2;
            // Carga el template de la página main
            $main = Template::render('home/home-body.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'GREETING' => $saludo,
                'MESSAGE1' => $message1,
                'MESSAGE2' => $message2,
            ], true);
            // Carga el template de proyectos main
            $proy = Template::render('home/home-proy.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'PROYECTOS' => $proyectos,
            ], true);
            // Carga el template del blog de pagina main
            $blog = Template::render('home/home-blog.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'BLOG' => $entradas,
            ], true);
            // Carga el template de contacto de pagina main
            //$contact = Template::render('home/home-cont.view.php', [
            //    'THIS_SERVER' => THIS_SERVER,
            //    'CONTACT1' => $contacto1,
            //    'CONTACT2' => $contacto2,
            //], true);

            return $main . $proy . $blog;
        }
        public function source() {
            return include __DIR__ . '/../controller/home/home-source.controller.php';
        }
    }
?>