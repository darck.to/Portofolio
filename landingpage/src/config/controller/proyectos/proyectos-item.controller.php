<?php
    class item{
        public static function run($id) {
            header("Access-Control-Allow-Origin: *");
            //header("Content-Type: application/json; charset=UTF-8");
            header("Content-Type: text/html; charset=utf-8");
            header("Access-Control-Allow-Methods: POST");
            header("Access-Control-Max-Age: 3600");
            header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

            // Entry calling from wp api headless-wordpress-1
            $item = array();
            $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/' . $id;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
            if($response = curl_exec($ch)) {
                curl_close($ch);
                $data = json_decode($response);
                $title = $data->title->rendered;
                $content = $data->content->rendered;
                // Tags
                $tags = array(["TAG"=>"No Tag"]);
                if (!empty($data->tags)) {
                    $tags = self::tags($data->tags);
                }
                // Date a esMx
                $date = $data->date;
                $date = date("d-m-Y", strtotime($date));
                // Get image from imageEmbedded()
                $foto = self::imageEmbedded($id);
                $item[] = array("success"=> true, "TITLE"=> $title, "CONTENT"=> $content, "DATE"=> $date, "IMG"=> $foto, "ALT"=> $title);
            } else {
                $item[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
            }

            http_response_code(200);
            return json_encode(array(
                                    "success" => true,
                                    "item" => $item,
                                    "tags" => $tags
                                ));

        }
        // Get image from imageEmbedded()
        private static function imageEmbedded($id) {
            $media = "/src/assets/img/error.webp";
            $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/' . $id . '?_embed';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
            if($response = curl_exec($ch)) {
                curl_close($ch);
                $data = json_decode($response);
                if (isset($data->_embedded->{'wp:featuredmedia'}[0]->source_url)) {
                    $media = $data->_embedded->{'wp:featuredmedia'}[0]->source_url;
                }
                return $media;
                
            }
            return false;
        }

        // Get tags from tags()
        private static function tags($tags) {
            $array = array();
            foreach ($tags as $tag) {
                $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/tags/' . $tag;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
                if($response = curl_exec($ch)) {
                    curl_close($ch);
                    $data = json_decode($response);
                    $array[] = array("TAG"=>$data->name, "COLOR"=>$data->slug);
                }    
            }
            return $array;
        }
    }

?>