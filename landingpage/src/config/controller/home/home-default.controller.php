<?php
    header("Access-Control-Allow-Origin: *");
    //header("Content-Type: application/json; charset=UTF-8");
    header("Content-Type: text/html; charset=utf-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    // Proyectos calling from wp api headless-wordpress-1
    $proyectos = array();
    $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/?categories=4&per_page=8';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
    if($response = curl_exec($ch)) {
        curl_close($ch);
        $data = json_decode($response);
        foreach($data as $item) {
            $id = $item->id;
            $title = $item->title->rendered;
            $foto = imageEmbedded($item->id);
            $proyectos[] = array("success"=> true, "TITLE"=> $title, "IMG"=> $foto, "URL"=> $id, "ALT"=> $title);
        }
    }   else {
        $proyectos[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
    }
    // Blog
    $blog = array();
    // Order by date
    $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/?_fields=author,id,excerpt,title,date&categories=5&order=desc&per_page=4';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
    if($response = curl_exec($ch)) {
        curl_close($ch);
        $data = json_decode($response);
        foreach($data as $item) {
            $id = $item->id;
            $title = $item->title->rendered;
            $fecha = $item->date;
            $fecha = date("d-m-Y", strtotime($fecha));
            $nota = $item->excerpt->rendered;
            $blog[] = array("success"=> true, "TITLE"=> $title, "CONTENT"=> $nota, "URL"=> $id, "DATE"=> $fecha);
        }
    }   else {
        $blog[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
    }
    // Contact
    $contact1[] = array("URL" =>"https://www.linkedin.com/in/unepic/", "ICON" =>"logo-linkedin", "NAME" =>"LinkedIn");
    $contact1[] = array("URL" =>"https://twitter.com/_robertovaldez", "ICON" =>"logo-twitter", "NAME" =>"Twitter");
    $contact1[] = array("URL" =>"https://github.com/nexuzjaja", "ICON" =>"logo-github", "NAME" =>"GitHub");
    $contact2[] = array("URL" =>"https://api.whatsapp.com/send/?phone=5214941055849", "ICON" =>"logo-whatsapp", "NAME" =>"WhatsApp");
    $contact2[] = array("URL" =>"https://codepen.io/Manuel-Roberto-Valdez-Guti-rrez", "ICON" =>"logo-codepen", "NAME" =>"CodePen");
    $contact2[] = array("URL" =>"https://gitlab.com/darck.to", "ICON" =>"logo-gitlab", "NAME" =>"GitLab");
    http_response_code(200);
    return json_encode(array(
                            "greeting" => "Soy Roberto Valdez!",
                            "message1" => "Soy un Full Stack Developer, tengo mas de 15 años de experiencia en el manejo de tecnologías como PHP, JavaScript, HTML, CSS, MySQL, entre otras.",
                            "message2" => "Quiere decir que empec&eacute; a hacer p&aacute;ginas web y ahora las puedo hacer m&aacute;s grandes :)",
                            "proyectos" => $proyectos,
                            "blog" => $blog,
                            "contact1" => $contact1,
                            "contact2" => $contact2
                        ));


    // Get image from imageEmbedded()
    function imageEmbedded($id) {
        $media = "/src/assets/img/error.webp";
        $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/' . $id . '?_embed';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
        if($response = curl_exec($ch)) {
            curl_close($ch);
            $data = json_decode($response);
            if (isset($data->_embedded->{'wp:featuredmedia'}[0]->source_url)) {
                $media = $data->_embedded->{'wp:featuredmedia'}[0]->source_url;
            }
            return $media;
            
        }
        return false;
    }

?>