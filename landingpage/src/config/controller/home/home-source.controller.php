<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    //header("Content-Type: text/html; charset=utf-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    // Chek if comes from js $.POST
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        // Session start
        session_start();
        $_SESSION['source'] = 1;
        http_response_code(200);
        return true;
    } else {
        $_SESSION['source'] = NULL;
        // Session destroy
        session_destroy();
        http_response_code(404);
        return false;
    }
    
?>