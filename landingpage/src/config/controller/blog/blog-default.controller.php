<?php
    header("Access-Control-Allow-Origin: *");
    //header("Content-Type: application/json; charset=UTF-8");
    header("Content-Type: text/html; charset=utf-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    // Proyectos calling from wp api headless-wordpress-1
    $proyectos = array();
    $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/?_fields=id,title,link&categories=4&per_page=5';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
    if($response = curl_exec($ch)) {
        curl_close($ch);
        $data = json_decode($response);
        foreach($data as $item) {
            $id = $item->id;
            $title = $item->title->rendered;
            $foto = imageEmbedded($item->id);
            $proyectos[] = array("success"=> true, "TITLE"=> $title, "IMG"=> $foto, "URL"=> $id, "ALT"=> $title);
        }
    }   else {
        $proyectos[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
    }
    // Blog
    $blog = array();
    // Order by date
    $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/?_fields=author,id,excerpt,title,date&categories=5&order=desc';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
    if($response = curl_exec($ch)) {
        curl_close($ch);
        $data = json_decode($response);
        // Cuenta cuantos elementos hay en data
        $count = count($data);
        foreach($data as $item) {
            $id = $item->id;
            $title = $item->title->rendered;
            $fecha = $item->date;
            $fecha = date("d-m-Y", strtotime($fecha));
            $nota = $item->excerpt->rendered;
            $blog[] = array("success"=> true, "TITLE"=> $title, "CONTENT"=> $nota, "URL"=> $id, "DATE"=> $fecha);
        }
    }   else {
        $blog[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
    }
    return json_encode(array(
                            "success" => true,
                            "cuantos" => $count,
                            "blog" => $blog,
                            "proyectos" => $proyectos
                        ));
    
    // Get image from imageEmbedded()
    function imageEmbedded($id) {
        $media = "/img/logo-wide.png";
        $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/' . $id . '?_embed';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
        if($response = curl_exec($ch)) {
            curl_close($ch);
            $data = json_decode($response);
            if (isset($data->_embedded->{'wp:featuredmedia'}[0]->source_url)) {
                $media = $data->_embedded->{'wp:featuredmedia'}[0]->source_url;
            }
            return $media;
            
        }
        return false;
    }

?>