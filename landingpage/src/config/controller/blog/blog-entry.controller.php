<?php
    class entry{
        public static function run($id) {
            header("Access-Control-Allow-Origin: *");
            //header("Content-Type: application/json; charset=UTF-8");
            header("Content-Type: text/html; charset=utf-8");
            header("Access-Control-Allow-Methods: POST");
            header("Access-Control-Max-Age: 3600");
            header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

            // Entry calling from wp api headless-wordpress-1
            $entry = array();
            $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/' . $id;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
            if($response = curl_exec($ch)) {
                curl_close($ch);
                $data = json_decode($response);
                $title = $data->title->rendered;
                $content = $data->content->rendered;
                // Date a esMx
                $date = $data->date;
                $date = date("d-m-Y", strtotime($date));
                // Get image from imageEmbedded()
                $foto = self::imageEmbedded($id);
                $entry[] = array("success"=> true, "TITLE"=> $title, "CONTENT"=> $content, "DATE"=> $date, "IMG"=> $foto, "ALT"=> $title);
            } else {
                $entry[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
            }

            http_response_code(200);
            return json_encode(array(
                                    "success" => true,
                                    "entry" => $entry
                                ));

        }
        // Get image from imageEmbedded()
        private static function imageEmbedded($id) {
            $media = "/src/assets/img/error.webp";
            $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/' . $id . '?_embed';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
            if($response = curl_exec($ch)) {
                curl_close($ch);
                $data = json_decode($response);
                if (isset($data->_embedded->{'wp:featuredmedia'}[0]->source_url)) {
                    $media = $data->_embedded->{'wp:featuredmedia'}[0]->source_url;
                }
                return $media;
                
            }
            return false;
        }
    }

?>