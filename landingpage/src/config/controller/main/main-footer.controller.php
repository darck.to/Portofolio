<?php
    header("Access-Control-Allow-Origin: *");
    //header("Content-Type: application/json; charset=UTF-8");
    header("Content-Type: text/html; charset=utf-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
    // Contact
    $contact1[] = array("URL" =>"https://www.linkedin.com/in/unepic/", "ICON" =>"logo-linkedin", "NAME" =>"LinkedIn");
    $contact1[] = array("URL" =>"https://twitter.com/_robertovaldez", "ICON" =>"logo-twitter", "NAME" =>"Twitter");
    $contact1[] = array("URL" =>"https://github.com/nexuzjaja", "ICON" =>"logo-github", "NAME" =>"GitHub");
    $contact2[] = array("URL" =>"https://api.whatsapp.com/send/?phone=5214941055849", "ICON" =>"logo-whatsapp", "NAME" =>"WhatsApp");
    $contact2[] = array("URL" =>"https://codepen.io/Manuel-Roberto-Valdez-Guti-rrez", "ICON" =>"logo-codepen", "NAME" =>"CodePen");
    $contact2[] = array("URL" =>"https://gitlab.com/darck.to", "ICON" =>"logo-gitlab", "NAME" =>"GitLab");
    http_response_code(200);
    return json_encode(array(
                            "contact1" => $contact1,
                            "contact2" => $contact2
                        ));
?>