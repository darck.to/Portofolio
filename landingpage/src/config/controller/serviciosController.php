<?php
    class serviciosController {
        public function home() {
            // Carga template main
            return Template::home('Blog mrvg.DEV', 'servicios/servicios');
        }
        // Carga template de la servicios principal
        public function main() {
            // Carga el template de la página main
            $template = require __DIR__ . '/./servicios/servicios-default.controller.php';
            $template = json_decode($template);

            // Carga el template del servicios de pagina main
            $servicios = Template::render('servicios/servicios-main.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'SERVICIOS' => $template->servicios
            ], true);
            return $servicios;
        }
        // Carga template de la entrada de servicios
        public function entry($id) {
            // Si viene desde navegador carga el template de la página main
            session_start();
            if (!isset($_SESSION['source'])) {
                // Carga template main
                $main = Template::home('Servicios mrvg.DEV', 'servicios/servicios-entry');
                // Carga el entry template
                require __DIR__ . '/./servicios/servicios-entry.controller.php';
                $template = entry::run($id);
                $template = json_decode($template);
                
                // Carga el template del servicios de pagina main
                $servicios = Template::render('servicios/servicios-entry-full.view.php', [
                    'THIS_SERVER' => THIS_SERVER,
                    'ENTRY' => $template->entry,
                    'TAGS' => $template->tags,
                ], true);
    
                return $servicios . $main;

            } else {
                session_destroy();
                // Carga el template de la página main
                require __DIR__ . '/./servicios/servicios-entry.controller.php';
                $template = entry::run($id);
                $template = json_decode($template);
                
                // Carga el template del servicios de pagina main
                $servicios = Template::render('servicios/servicios-entry.view.php', [
                    'THIS_SERVER' => THIS_SERVER,
                    'ENTRY' => $template->entry,
                ], true);
    
                return $servicios;
            }
        }
    }
?>