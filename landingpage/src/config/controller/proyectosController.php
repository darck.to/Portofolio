<?php
    class proyectosController {
        public function home() {
            // Carga template main
            return Template::home('Proyectos mrvg.DEV', 'proyectos/proyectos');
        }
        // Carga template de la proyectos principal
        public function main() {
            // Carga el template de la página main
            $template = require __DIR__ . '/./proyectos/proyectos-default.controller.php';
            $template = json_decode($template);

            // Carga el template del proyectos de pagina main
            $proyectos = Template::render('proyectos/proyectos-main.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'CUANTOS' => $template->cuantos,
                'PROYECTOS' => $template->proyectos,
            ], true);
            return $proyectos;
        }
        // Carga template del proyecto individual
        public function item($id) {
            // Si viene desde navegador carga el template de la página main
            session_start();
            if (!isset($_SESSION['source'])) {
                // Carga template main
                $main = Template::home('Proyectos mrvg.DEV', 'proyectos/proyectos-item');
                // Carga el item  template
                require __DIR__ . '/./proyectos/proyectos-item.controller.php';
                $template = item::run($id);
                $template = json_decode($template);
                
                // Carga el template del proyectos de pagina main
                $proyecto = Template::render('proyectos/proyectos-item-full.view.php', [
                    'THIS_SERVER' => THIS_SERVER,
                    'ITEM' => $template->item,
                    'TAGS' => $template->tags,
                ], true);
                return $proyecto . $main;

            } else {
                session_destroy();
                // Carga el template de la página main
                require __DIR__ . '/./proyectos/proyectos-item.controller.php';
                $template = item::run($id);
                $template = json_decode($template);
                
                // Carga el template del proyectos de pagina main
                $proyecto = Template::render('proyectos/proyectos-item.view.php', [
                    'THIS_SERVER' => THIS_SERVER,
                    'ITEM' => $template->item,
                    'TAGS' => $template->tags,
                ], true);
                
                return $proyecto;
            }
        }
    }
?>