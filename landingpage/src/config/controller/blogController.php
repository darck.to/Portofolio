<?php
    class blogController {
        public function home() {
            // Carga template main
            return Template::home('Blog mrvg.DEV', 'blog/blog');
        }
        // Carga template de la blog principal
        public function main() {
            // Carga el template de la página main
            $template = require __DIR__ . '/./blog/blog-default.controller.php';
            $template = json_decode($template);

            // Carga el template del blog de pagina main
            $blog = Template::render('blog/blog-main.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'CUANTOS' => $template->cuantos,
                'BLOG' => $template->blog,
                'PROYECTOS' => $template->proyectos,
            ], true);
            return $blog;
        }
        // Carga template de la entrada de blog
        public function entry($id) {
            // Si viene desde navegador carga el template de la página main
            session_start();
            if (!isset($_SESSION['source'])) {
                // Carga template main
                $main = Template::home('Blog mrvg.DEV', 'blog/blog-entry');
                // Carga el entry template
                require __DIR__ . '/./blog/blog-entry.controller.php';
                $template = entry::run($id);
                $template = json_decode($template);
                
                // Carga el template del blog de pagina main
                $blog = Template::render('blog/blog-entry-full.view.php', [
                    'THIS_SERVER' => THIS_SERVER,
                    'ENTRY' => $template->entry,
                ], true);
    
                return $blog . $main;

            } else {
                session_destroy();
                // Carga el template de la página main
                require __DIR__ . '/./blog/blog-entry.controller.php';
                $template = entry::run($id);
                $template = json_decode($template);
                
                // Carga el template del blog de pagina main
                $blog = Template::render('blog/blog-entry.view.php', [
                    'THIS_SERVER' => THIS_SERVER,
                    'ENTRY' => $template->entry,
                ], true);
    
                return $blog;
            }
        }
    }
?>