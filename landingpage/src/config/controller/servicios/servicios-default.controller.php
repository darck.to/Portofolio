<?php
    header("Access-Control-Allow-Origin: *");
    //header("Content-Type: application/json; charset=UTF-8");
    header("Content-Type: text/html; charset=utf-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    // Servicios calling from wp api headless-wordpress-1
    $servicios = array();
    $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/?categories=18';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
    if($response = curl_exec($ch)) {
        curl_close($ch);
        $data = json_decode($response);
        // Cuenta cuantos elementos hay en data
        $count = count($data);
        foreach($data as $item) {
            $id = $item->id;
            $title = $item->title->rendered;
            $foto = imageEmbedded($item->id);
            $servicios[] = array("success"=> true, "TITLE"=> $title, "IMG"=> $foto, "URL"=> $id, "ALT"=> $title);
        }
    }   else {
        $servicios[] = array("success"=> false, "message"=> "Fetch error " . error_get_last() );
    }

    return json_encode(array(
                            "success" => true,
                            "servicios" => $servicios
                        ));
    
    // Get image from imageEmbedded()
    function imageEmbedded($id) {
        $media = "/src/assets/img/error.webp";
        $url = 'https://ciberkiosco.com/cdn/wp-json/wp/v2/posts/' . $id . '?_embed';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: PHP'));
        if($response = curl_exec($ch)) {
            curl_close($ch);
            $data = json_decode($response);
            if (isset($data->_embedded->{'wp:featuredmedia'}[0]->source_url)) {
                $media = $data->_embedded->{'wp:featuredmedia'}[0]->source_url;
            }
            return $media;
            
        }
        return false;
    }

?>