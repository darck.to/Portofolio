<?php
    class errorController {
        public function default() {
            include __DIR__ . '/../controller/error/error-default.controller.php';
        }
        public function empty() {
            include __DIR__ . '/../controller/error/error-404.controller.php';
        }
    }
?>