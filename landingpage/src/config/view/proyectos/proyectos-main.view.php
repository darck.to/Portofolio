<div class="container is-max-widescreen">
    <section class="section">
        <nav class="level">
            <!-- Left side -->
            <div class="level-left title has-text-weight-light has-text-centered p-4 has-background-blue-grey is-relative">
                <figure class="image box-logo">
                    <img src="{{THIS_SERVER}}/src/assets/img/logo.png" alt="logo">
                </figure>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center is-size-4">
                        <ion-icon name="home" class="mr-2"></ion-icon>
                        Inicio |&nbsp;
                    </a>
                </div>
                <div class="level-item">
                    <p class="subtitle is-5 has-text-white">
                        <strong class="has-text-white">{{CUANTOS}}</strong> proyectos
                    </p>
                </div>
                <div class="level-item">
                    <div class="field has-addons">
                        <p class="control">
                            <input class="input" type="text" placeholder="Find a post">
                        </p>
                        <p class="control">
                            <button class="button">
                                Buscar
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </nav>
        <div class="columns">
            <div class="column is-offset-1 is-10">
                <h1 class="title has-text-white has-text-weight-light has-text-centered p-4 has-background-red">Mis Proyectos</h1>
                <div class="columns is-multiline mt-6">

                    {{START_PROYECTOS}}
                    <div class="column is-3 has-border-1-grey-light">
                        <a class="proy-link" href="{{THIS_SERVER}}/proyectos/item/{{PROYECTOS_URL}}" data-id="{{PROYECTOS_URL}}" data-ruta="/proyectos/item/">
                            <figure class="image is-3by2">
                                <img class="is-img-ofcover is-hover-blur is-hover-zoom" src="{{PROYECTOS_IMG}}" alt="{{PROYECTOS_ALT}}">
                            </figure>
                            <h1 class="title has-color-white-ter has-text-weight-light has-text-centered is-size-5 pt-2">{{PROYECTOS_TITLE}}</h1>
                        </a>
                    </div>
                    {{END_PROYECTOS}}
                    
                </div>
            </div>
        </div>
    </section>
</div>