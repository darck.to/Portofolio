<div class="container">
    <div class="box">
        <div class="columns">
            {{START_ITEM}}
            <div class="column">
                <figure class="image is-16by9">
                    <img src="{{ITEM_IMG}}" alt="{{ITEM_ALT}}" class="is-img-ofcover">
                </figure>
                {{START_TAGS}}
                <span class="tag {{TAGS_COLOR}}">{{TAGS_TAG}}</span>
                {{END_TAGS}}
                <h1 class="title is-2 has-text-centered">{{ITEM_TITLE}}</h1>
                <div class="content">
                    {{ITEM_CONTENT}}
                </div>
            </div>
            {{END_ITEM}}
        </div>
    </div>
</div>