<div class="container is-max-widescreen">
    <section class="section">
        <nav class="level">
            <!-- Left side -->
            <div class="level-left has-text-centered p-4 has-background-blue-grey is-relative">
                <figure class="image box-logo">
                    <img src="{{THIS_SERVER}}/src/assets/img/logo.png" alt="logo">
                </figure>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center is-size-4">
                        <ion-icon name="home" class="mr-2"></ion-icon>
                        Inicio |&nbsp;
                    </a>
                </div>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/proyectos/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center">
                        <ion-icon size="large" name="arrow-back-outline"></ion-icon>
                        Proyectos
                    </a>
                </div>
            </div>
        </nav>
            
        <div class="box">
            <div class="columns">
                {{START_ITEM}}
                <div class="column">
                    <figure class="image is-16by9">
                        <img src="{{ITEM_IMG}}" alt="{{ITEM_ALT}}" class="is-img-ofcover">
                    </figure>
                    {{START_TAGS}}
                    <span class="tag {{TAGS_COLOR}}">{{TAGS_TAG}}</span>
                    {{END_TAGS}}
                    <h1 class="title is-2 has-text-centered">{{ITEM_TITLE}}</h1>
                    <div class="content">
                        {{ITEM_CONTENT}}
                    </div>
                </div>
                {{END_ITEM}}
            </div>
        </div>

    </section>
</div>