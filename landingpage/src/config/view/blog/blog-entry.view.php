<div class="container">
    <div class="box">
        <div class="columns">
            {{START_ENTRY}}
            <div class="column">
                <figure class="image is-16by9">
                    <img src="{{ENTRY_IMG}}" alt="{{ENTRY_ALT}}" class="is-img-ofcover">
                </figure>
                <small>{{ENTRY_DATE}}</small>
                <h1 class="title is-1">{{ENTRY_TITLE}}</h1>
                <div class="content">
                    {{ENTRY_CONTENT}}
                </div>
            </div>
            {{END_ENTRY}}
        </div>
    </div>
</div>