<div class="container is-max-widescreen">
    <section class="section">
        <nav class="level">
            <!-- Left side -->
            <div class="level-left has-text-centered p-4 has-background-blue-grey is-relative">
                <figure class="image box-logo">
                    <img src="{{THIS_SERVER}}/src/assets/img/logo.png" alt="logo">
                </figure>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center is-size-4">
                        <ion-icon name="home" class="mr-2"></ion-icon>
                        Inicio |&nbsp;
                    </a>
                </div>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/blog/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center">
                        <ion-icon size="large" name="arrow-back-outline" class="mr-2"></ion-icon>
                        Blog
                    </a>
                </div>
            </div>
        </nav>

        <div class="box">
            <div class="columns">
                {{START_ENTRY}}
                <div class="column">
                    <figure class="image is-16by9">
                        <img src="{{ENTRY_IMG}}" alt="{{ENTRY_ALT}}" class="is-img-ofcover">
                    </figure>
                    <small>{{ENTRY_DATE}}</small>
                    <h1 class="title is-1">{{ENTRY_TITLE}}</h1>
                    <div class="content">
                        {{ENTRY_CONTENT}}
                    </div>
                </div>
                {{END_ENTRY}}
            </div>
        </div>

    </section>
</div>