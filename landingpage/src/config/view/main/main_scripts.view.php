<script src="{{THIS_SERVER}}/css/ionicons/ionicons.esm.js" type="module"></script>
<script nomodule src="{{THIS_SERVER}}/css/ionicons/ionicons.js"></script>

<script src="{{THIS_SERVER}}/src/js/vendor/particles.min.js"></script>

<script src="{{THIS_SERVER}}/src/js/vendor/jquery-3.7.1.min.js"></script>
<script src="{{THIS_SERVER}}/src/js/vendor/modernizr-3.11.2.min.js"></script>
<script src="{{THIS_SERVER}}/src/js/index.js" type="module"></script>
<script src="{{THIS_SERVER}}/src/js/{{SCRIPT}}.js" type="module"></script>

