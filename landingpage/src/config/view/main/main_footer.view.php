<footer id="contacto-nav" class="footer has-background-blue-grey-lighter hero-scroll">
    <section class="hero is-fullheight">
        <div class="hero-body">
        
            <div class="container">
                <h1 class="title has-text-white has-text-weight-light has-text-centered p-4 has-background-blue-grey">
                    Contr&aacute;tame!
                </h1>
                <div class="columns is-multiline">
                    <ul class="column is-offset-3 is-4">
                    {{START_CONTACT1}}
                        <li class="has-text-white-ter">
                            <a href="{{CONTACT1_URL}}" target="_blank" class="is-size-3">
                                <span class="icon">
                                    <ion-icon name="{{CONTACT1_ICON}}"></ion-icon>
                                </span>
                                {{CONTACT1_NAME}}
                            </a>
                        </li>
                        {{END_CONTACT1}}
                    </ul>
                    <ul class="column is-4">
                    {{START_CONTACT2}}
                        <li class="has-text-white-ter">
                            <a href="{{CONTACT2_URL}}" target="_blank" class="is-size-3">
                                <span class="icon">
                                    <ion-icon name="{{CONTACT2_ICON}}"></ion-icon>
                                </span>
                                {{CONTACT2_NAME}}
                            </a>
                        </li>
                        {{END_CONTACT2}}
                    </ul>
                </div>
                <div class="content has-text-centered">
                    <p class="has-text-white-ter">
                        <strong>@mrvg</strong> por <a href="{{THIS_SERVER}}">Manuel Roberto Valdez Guti&eacute;rrez</a>
                    </p>
                </div>
            </div>

        </div>
    </section>
</footer>