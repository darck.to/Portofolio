<!doctype html>
<html class="no-js has-background-grey-dark is-relative" lang="es">

  <head>
    <meta charset="utf-8">
    <title>{{TITLE}}</title>
    <meta name="description" content="{{THIS_DESCRIPTION}}">
    <meta name="keywords" content="{{THIS_KEYWORDS}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Open Graph Tags-->
    <meta property="og:title" content="{{TITLE}}">
    <meta property="og:description" content="{{THIS_DESCRIPTION}}">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{THIS_SERVER}}">
    <meta property="og:image"  itemprop="image" content="{{THIS_SERVER}}/src/assets/img/logo.png">
    
    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="twitter:domain" content="{{THIS_DOMAIN}}">
    <meta property="twitter:url" content="{{THIS_SERVER}}">
    <meta name="twitter:title" content="{{TITLE}}">
    <meta name="twitter:description" content="{{THIS_DESCRIPTION}}">
    <meta name="twitter:image" content="{{THIS_SERVER}}/src/assets/img/logo.png">

    <link rel="manifest" href="{{THIS_SERVER}}/site.webmanifest">
    <link rel="icon" type="image/png" href="{{THIS_SERVER}}/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{THIS_SERVER}}/favicon-16x16.png" sizes="16x16" />

    <link rel="apple-touch-icon" href="{{THIS_SERVER}}/icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{THIS_SERVER}}/css/normalize.css">
    <link rel="stylesheet" href="{{THIS_SERVER}}/css/main.css">

    <meta name="theme-color" content="#fafafa">
  </head>

  
  <body class="is-relative">
    <div id="particles-js" class="particles-background"></div>
    <div id="mainContainer">
    </div>
  </body>

</html>