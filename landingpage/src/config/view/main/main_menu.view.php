<span id="mainMenu" class="icon has-color-grey-lighter">
    <ion-icon class="nav-burguer is-pulse is-size-1 is-clickable is-hidden-desktop " name="grid"></ion-icon>
    <ion-icon class="nav-home is-size-1 is-clickable is-hidden-touch " name="code-working"></ion-icon>
    <div class="box nav-box has-background-white-ter">
        <aside class="menu">
          <p class="menu-label">
            Navegaci&oacute;n
          </p>
          <ul class="menu-list">
            <li><a class="nav-item" href="#acerca-nav" data-url="{{THIS_SERVER}}"/>Acerca de Mi</a></li>
            <li><a class="nav-item" href="#proyectos-nav" data-url="{{THIS_SERVER}}/proyectos">Proyectos</a></li>
            <li><a class="nav-item" href="#blog-nav" data-url="{{THIS_SERVER}}/blog">Blog</a></li>
            <li><a class="nav-item" href="#contacto-nav" data-url="{{THIS_SERVER}}/">Contacto</a></li>
            <li><a href="{{THIS_SERVER}}/servicios">Servicios</a></li>
          </ul>
        </aside>
    </div>
</span>
