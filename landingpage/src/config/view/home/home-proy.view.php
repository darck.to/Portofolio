<section id="proyectos-nav" class="hero is-fullheight hero-scroll">
    <div class="hero-body is-relative">
        
        <div class="container container is-max-widescreen">
            <h1 class="title has-color-white-ter has-text-weight-light has-text-centered">
                Estos son solo algunos de mis proyectos
            </h1>
            <h2 class="subtitle has-color-white-ter has-text-weight-light has-text-centered is-underlined">
                <a href="{{THIS_URL}}/proyectos/" class="is-flex is-justify-content-center is-align-items-center has-text-weight-bold">
                    Ver todos
                    <span class="icon has-color-red ml-3">
                        <ion-icon class="is-size-1" name="rocket"></ion-icon>
                    </span>
                </a>
            </h2>
            <div class="columns is-multiline">

                {{START_PROYECTOS}}
                <div class="column is-3 has-border-1-grey-light has-background-cheese">
                    <a class="proy-link" href="{{THIS_SERVER}}/proyectos/item/{{PROYECTOS_URL}}" data-id="{{PROYECTOS_URL}}" data-ruta="/proyectos/item/">
                        <figure class="image is-3by2">
                            <img class="is-img-grayscale is-img-ofcover is-hover-blur is-hover-zoom" src="{{PROYECTOS_IMG}}" alt="{{PROYECTOS_ALT}}">
                        </figure>
                        <h1 class="title has-color-white-ter has-text-weight-light has-text-centered is-size-5 pt-2">{{PROYECTOS_TITLE}}</h1>
                    </a>
                </div>
                {{END_PROYECTOS}}
                
            </div>
            
        </div>
        <a href="#blog-nav">
            <span class="is-flex is-flex-direction-column is-align-items-center has-color-white-ter is-size-1 next-portfolio">
                <span class="has-text-centered">Mi Blog</span>
                <ion-icon name="chevron-down-outline"></ion-icon>
            </span>
        </a>

    </div>
</section>