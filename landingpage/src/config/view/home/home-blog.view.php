<section id="blog-nav" class="hero is-fullheight hero-scroll">
    <div class="hero-body is-relative">
        
        <div class="container container is-max-widescreen">
            <h1 class="title has-color-white-ter has-text-weight-light has-text-centered">
                &Uacute;ltimos art&iacute;culos
            </h1>
            <h2 class="subtitle has-color-white-ter has-text-weight-light has-text-centered is-underlined">
                <a href="{{THIS_URL}}/blog/" class="is-flex is-justify-content-center is-align-items-center has-text-weight-bold">
                    Ir al Blog
                    <span class="icon has-color-blue-grey ml-3">
                        <ion-icon class="is-size-1" name="book"></ion-icon>
                    </span>
                </a>
            </h2>
            <div class="columns is-multiline">
    
                {{START_BLOG}}
                <div class="column is-offset-3 is-6">
                    <div class="block">
                        <a class="blog-link" href="{{THIS_SERVER}}/blog/entry/{{BLOG_URL}}" data-id="{{BLOG_URL}}" data-ruta="/blog/entry/">
                            <h1 class="title has-color-white-ter has-text-weight-light">{{BLOG_TITLE}}</h1>
                        </a>
                        <div class="has-color-white-ter">{{BLOG_CONTENT}}</div>
                        <p><date class="has-color-white-ter is-size-7">{{BLOG_DATE}}</date></p>
                    </div>
                </div>
                {{END_BLOG}}
                
            </div>
    
        </div>
        <a href="#contacto-nav">
            <span class="is-flex is-flex-direction-column is-align-items-center has-color-white-ter is-size-1 next-portfolio">
                <span class="has-text-centered">Mis Redes</span>
                <ion-icon name="chevron-down-outline"></ion-icon>
            </span>
        </a>
        
    </div>
</section>