<section id="acerca-nav" class="hero is-fullheight hero-scroll">
    <div class="hero-body is-relative">

        <div class="container is-max-widescreen">
    
            <div class="columns is-multiline">
                <div class="column is-offset-2 is-8 is-relative">
                    <figure class="image main-logo">
                        <img src="{{THIS_SERVER}}/src/assets/img/logo.png" alt="logo">
                    </figure>
                    <h1 class="title has-text-white has-text-weight-light has-text-centered p-4 has-background-red">{{GREETING}}</h1>
                    <span class="subtitle is-size-1 auto-script has-text-weight-bold has-color-white-ter p-3 has-background-blue-grey"></span>
                </div>
                <div class="column is-offset-3 is-6 has-text-justified">
                    <p class="has-color-white-ter">{{MESSAGE1}}</p>
                    <p class="has-color-white-ter mt-3">{{MESSAGE2}}</p>
                </div>
            </div>
    
            <a href="#proyectos-nav">
        </div>
        <span class="is-flex is-flex-direction-column is-align-items-center has-color-white-ter is-size-1 next-portfolio">
                <span class="has-text-centered">Mis Proyectos</span>
                <ion-icon name="chevron-down-outline"></ion-icon>
            </span>
        </a>

    </div>
</section>
