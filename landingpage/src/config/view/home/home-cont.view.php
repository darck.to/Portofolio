<section id="contacto-nav" class="hero is-fullheight hero-scroll">
    <div class="hero-body">
            
        <div class="container container is-max-widescreen">
            <h1 class="title has-text-white has-text-weight-light has-text-centered p-4 has-background-red">
                Contratame!
            </h1>
            <div class="columns is-multiline">
                <ul class="column is-offset-3 is-4">
                {{START_CONTACT1}}
                    <li class="has-text-white-ter">
                        <a href="{{CONTACT1_URL}}" class="is-size-3">
                            <span class="icon">
                                <ion-icon name="{{CONTACT1_ICON}}"></ion-icon>
                            </span>
                            {{CONTACT1_NAME}}
                        </a>
                    </li>
                    {{END_CONTACT1}}
                </ul>
                <ul class="column is-4">
                {{START_CONTACT2}}
                    <li class="has-text-white-ter">
                        <a href="{{CONTACT2_URL}}" class="is-size-3">
                            <span class="icon">
                                <ion-icon name="{{CONTACT2_ICON}}"></ion-icon>
                            </span>
                            {{CONTACT2_NAME}}
                        </a>
                    </li>
                    {{END_CONTACT2}}
                </ul>
            </div>

        </div>

    </div>
</section>
