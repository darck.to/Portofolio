<div class="container is-max-widescreen">
    <section class="section">
        <nav class="level">
            <!-- Left side -->
            <div class="level-left title has-text-weight-light has-text-centered p-4 has-background-blue-grey is-relative">
                <figure class="image box-logo">
                    <img src="{{THIS_SERVER}}/src/assets/img/logo.png" alt="logo">
                </figure>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center is-size-4">
                        <ion-icon name="home" class="mr-2"></ion-icon>
                        Inicio |&nbsp;
                    </a>
                </div>
            </div>
        </nav>
        <div class="columns">
            <div class="column is-offset-1 is-10">
                <h1 class="title has-text-white has-text-weight-light has-text-centered p-4 has-background-red">Servicios</h1>
                <div class="columns is-multiline mt-6">

                    {{START_SERVICIOS}}
                    <div class="column is-6 has-border-1-grey-light">
                        <a class="serv-link" href="{{THIS_SERVER}}/servicios/entry/{{SERVICIOS_URL}}" data-id="{{SERVICIOS_URL}}" data-ruta="/servicios/entry/">
                            <figure class="image is-3by2">
                                <img class="is-img-ofcover is-hover-blur is-hover-zoom" src="{{SERVICIOS_IMG}}" alt="{{SERVICIOS_ALT}}">
                            </figure>
                            <h1 class="title has-color-white-ter has-text-weight-light has-text-centered is-size-5 pt-2">{{SERVICIOS_TITLE}}</h1>
                        </a>
                    </div>
                    {{END_SERVICIOS}}
                    
                </div>
            </div>
        </div>
    </section>
</div>