<div class="container is-max-widescreen">
    <section class="section">
        <nav class="level">
            <!-- Left side -->
            <div class="level-left has-text-centered p-4 has-background-blue-grey is-relative">
                <figure class="image box-logo">
                    <img src="{{THIS_SERVER}}/src/assets/img/logo.png" alt="logo">
                </figure>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center is-size-4">
                        <ion-icon name="home" class="mr-2"></ion-icon>
                        Inicio |&nbsp;
                    </a>
                </div>
                <div class="level-item">
                    <a href="{{THIS_SERVER}}/servicios/" class="title has-text-weight-light has-color-white-ter is-flex is-align-items-center">
                        <ion-icon size="large" name="arrow-back-outline"></ion-icon>
                        Servicios
                    </a>
                </div>
            </div>
        </nav>
            
        <div class="box">
            <div class="columns">
                {{START_ENTRY}}
                <div class="column">
                    {{START_TAGS}}
                    <span class="tag {{TAGS_COLOR}}">{{TAGS_TAG}}</span>
                    {{END_TAGS}}
                    <h1 class="title is-2 has-text-centered">{{ENTRY_TITLE}}</h1>
                    <small>{{ENTRY_DATE}}</small>
                    <div class="content">
                        {{ENTRY_CONTENT}}
                    </div>
                </div>
                {{END_ENTRY}}
            </div>
        </div>

    </section>
</div>