<?php
    class Template{
        // Template agrega header y script correspondientes a cada call
        public static function home($title,$script) {
            $head = Template::render('main/main_headers.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'TITLE' => $title,
                'THIS_SERVER' => THIS_SERVER,
                'THIS_DOMAIN' => THIS_DOMAIN,
                'THIS_DESCRIPTION' => 'Portfolio de proyectos de desarrollo web y aplicaciones móviles',
                'THIS_KEYWORDS' => 'portfolio, proyectos, desarrollo web, desarrollo móvil, aplicaciones móviles, aplicaciones web, desarrollo de software, desarrollo de aplicaciones, desarrollo de aplicaciones móviles, desarrollo de aplicaciones web, desarrollo de software, desarrollo de software a medida, desarrollo de software a medida en mexico, desarrollo de software a medida en cdmx, desarrollo de software a medida en jalisco, desarrollo de software a medida en usa',
                'TITLE' => 'Portfolio mrvg.DEV'
                ], true);
            // Footer from main_footer.view.php
            $template = require __DIR__ . '/../controller/main/main-footer.controller.php';
            $template = json_decode($template);
            $contacto1 = $template->contact1;
            $contacto2 = $template->contact2;
            
            $footer = Template::render('main/main_footer.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'CONTACT1' => $contacto1,
                'CONTACT2' => $contacto2,
            ], true);
            $scripts = Template::render('main/main_scripts.view.php', [
                'THIS_SERVER' => THIS_SERVER,
                'SCRIPT' => $script
            ], true);
            // Menu de navegación
            $menu = Template::render('main/main_menu.view.php', [
                'THIS_SERVER' => THIS_SERVER,
            ], true);

            return $head . $menu . $footer . $scripts;
        }
        // Template invoca $file, intercambia {{}} por valores de $datos, also catches errors to return 404 if any
        public static function render($template, $datos, $caseInsensitive = false ) {
            $file = './src/config/view/' . $template;
            // Check if file exists or its empty
            if (! file_exists($file) || ! is_readable($file) || ! is_file($file) || ! filesize($file)) {
                return '<code>El archivo Plantilla es inexistente/vacio</code>';
            }
        
            $CI = $caseInsensitive ? 'i' : '';
        
            $contenido = file_get_contents( $file );
            foreach ($datos as $key => $value) {
                // En caso de array lo recorre para insertar varios elementos y no este vacio o null
                if (is_array($value) && !empty($value) && !is_null($value)) {
                    // El nombre del key es el nombre del template a multiplicar
                    $name = strtoupper($key);
                    $cntToMultiply = "";
                    // Busca el inicio y fin del template a multiplicar y lo agrega y reemplaza en la variable contenido
                    $startStr = "{{START_" . $name . "}}";
                    $endStr = "{{END_" . $name . "}}";
                    $startPos = strpos($contenido, $startStr);
                    $endPos = strpos($contenido, $endStr, $startPos + strlen($startStr));
                    foreach ($value as $index => $val) {
                        $cntToMultiply .= substr($contenido, $startPos + strlen($startStr), $endPos - $startPos - strlen($startStr));
                        foreach ($val as $key2 => $value2) {
                            $cntToMultiply = preg_replace("/{{\s*(" . $name. "_" . $key2 . ")\s*?}}/$CI", $value2, $cntToMultiply);
                        }
                    }
                    $contenido = substr_replace($contenido, $cntToMultiply, $startPos, $endPos - $startPos  + strlen($endStr));
                } else {
                    // Si es un solo elemento lo reemplaza directamente
                    $contenido = preg_replace("/{{\s*(" . $key . ")\s*?}}/$CI", $value, $contenido);
                }
            }
            // Si quedaron elementos {{}} sin reemplazar los elimina
            $contenido = preg_replace("/{{\s*([a-zA-Z0-9_]+)\s*?}}/$CI", "", $contenido);
            return $contenido;
        }
    }
?>