<?php
    $dirPath = dirname($_SERVER['SCRIPT_NAME']);
    $urlPath = $_SERVER['REQUEST_URI'];
    $url = substr($urlPath,strlen($dirPath));

    define('URL', $url);

    // Obtiene absolute path and url of the domain
    $server = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    // Definimos la constante THIS_SERVER
    define('THIS_SERVER', $server . '' . $_SERVER['HTTP_HOST']);
    // Definimos la constante THIS_DOMAIN
    define('THIS_DOMAIN', $_SERVER['HTTP_HOST']);
?>