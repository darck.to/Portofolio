<?php
    class Router {
        private $controller;
        private $method;
        private $destiny;
        private $controllerror;
        private $methoderror;
        private $error;

        public function __construct() {
            $this->Route();
        }

        public function Route() {
            // Catch errors and show them
            //error_reporting(E_ALL);
            //ini_set('display_errors', '1');

            $url = explode('/', URL);

            $this->controller = !empty($url[0]) ? $url[0] : 'home';
            $this->method = !empty($url[1]) ? $url[1] : 'home';
            $this->destiny = !empty($url[2]) ? $url[2] : null;
            //var_dump($this->controller, $this->method, $this->destiny);

            // Si controller y method son /src/cdn redirects to wp-json api rest
            if ($this->controller == 'src' && $this->method == 'cdn' && $$this->destiny == 'wp-json') {
                header('Location: ' . URL);
            }

            $this->controller = $this->controller . 'Controller';

            // Si existe la tercer entrada
            
            if (!file_exists(__DIR__ . '/../controller/' . $this->controller . '.php')) {
                //return 'No existe el controlador: ' . $this->controller;
                $this->error = 'default';
                $this->errorRouter();
            } else {
                require_once __DIR__ . '/../controller/' . $this->controller . '.php';
            }
        }

        public function run() {
            // Si existe el metodo
            if (!method_exists($this->controller, $this->method)) {
                //return 'No existe el metodo: ' . $this->controller . '>>' . $this->method;
                $this->error = 'empty';
                $this->errorRouter();
            } else {
                $controller = new $this->controller;
                $method = $this->method;
                $destiny = $this->destiny;
                return $controller->$method($destiny);
            }
        }

        // private function errorRouter
        private function errorRouter() {
            require_once __DIR__ . '/../controller/errorController.php';
            $this->controllerror = 'errorController';
            $controllerror = new $this->controllerror;
            $methoderror = $this->error;
            return $controllerror->$methoderror();
        }
    }
?>